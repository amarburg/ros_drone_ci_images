# Ros Drone Ci Images

"Optimized" Docker images for use with [ros_drone_ci](https://gitlab.com/amarburg/ros_drone_ci).

Starts with [published ROS images](https://hub.docker.com/_/ros), then pre-installs necessary packages (compilers, catkin, etc) with `apt`
